import 'dart:io';
import 'dart:math';

void main() {
  print(
      "Выберите режим игры (введите цифры):\n1. Вы загадываете, компьютер отгадывает   2. Компьютер загадывает, вы отгадываете\n3. Играть по очереди");
  String choice = stdin.readLineSync() ?? '';

  if (choice == '1') {
    computerGuesses();
  } else if (choice == '2') {
    userGuesses();
  } else if (choice == '3') {
    gameWithRounds();
  } else {
    print("Вы ввели неправильную комбинацию. Попробуйте еще раз!");
  }
}

void gameWithRounds() {
  int computer = 0;
  int user = 0;
  print("Выберите от 1 до 10 сколько раундов играть:");

  int round = int.tryParse(stdin.readLineSync()!) ?? 3;
  print("Отлично, играем раундов: $round");
  for (int i = 0; i < round; i++) {
    print("============${i + 1}й раунд============");
    int computerStep = computerGuesses();
    int userStep = userGuesses();

    if (computerStep > userStep) {
      user++; // Увеличиваем преимущество юзера, так как пользователь отгадал за наименьшее количество шагов
    } else if (computerStep < userStep) {
      computer++; // Компьютер отгадал за наименьшее кол-во шагов
    }
  }

  if (computer > user) {
    print("Выиграл компьютер!");
    printStat(computer, user);
  } else if (computer < user) {
    print("Вы выиграли!");
    printStat(computer, user);
  } else {
    print("Ничья!");
    printStat(computer, user);
  }
}

void printStat(int computer, int user) {
  print(
      "Побед в раундах у компьютера $computer.\nПобед в раундах у вас $user.");
}

int computerGuesses() {
  print(
      "\nИграет компьютер. Компьютер будет отгадывать! Загадавайте число от 1 до 100");

  String isItCorrectNumber = "";
  int min = 1;
  int max = 100;
  int guess;
  int step = 0;

  do {
    guess = (min + max) ~/ 2;
    print(
        'Компьютер предполагает, что ваше число: $guess. Введите "greater", "less" или "yes":');

    String answer = stdin.readLineSync() ?? '';
    switch (answer) {
      case "greater":
        min = guess + 1;
      case "less":
        max = guess - 1;
      case "yes":
        {
          print("Я - компьютер и я угадал!");
          isItCorrectNumber = "yes";
          break;
        }
      default:
        {
          print("Вы ввели неправильное слово! Попоробуйте еще раз!");
          continue;
        }
    }

    step++;
  } while (isItCorrectNumber != "yes");

  print("Компьютер отгадал за $step шагов");
  return step;
}

int userGuesses() {
  print(
      "\nИграет человек. Компьютер загадывает число от 1 до 100. Попробуйте угадать!");
  int randomNum = Random().nextInt(100);
  // print(randomNum);

  int userGuess;
  int step = 0;

  do {
    userGuess = int.tryParse(stdin.readLineSync() ?? '') ?? 0;
    if (userGuess < randomNum) {
      print("greater");
    } else if (userGuess > randomNum) {
      print("less");
    } else {
      print("yes");
      print(
          "Я загадывал число $randomNum, и вы отгадали, ваше число: $userGuess");
    }
    step++;
  } while (userGuess != randomNum);

  print("Вы отгадали за $step шагов");
  return step;
}
